package com.gitlab.docserver.api;

import java.util.Collection;

/**
 * Defines an artifact group
 */
public interface Group {
    /**
     *
     * @return the artifacts group id
     */
    String getGroupId();

    /**
     * This method will add a new {@link Artifact} if no one exists with
     * the given artifactId.
     *
     * @param artifactId used to lookup artifact within this group
     * @return the {@link Artifact} with given artifactId
     */
    Artifact getArtifact(String artifactId);

    /**
     *
     * @return all artifacts belonging to this group
     */
    Collection<Artifact> getArtifacts();
}
