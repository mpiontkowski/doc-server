package com.gitlab.docserver.api;

import java.io.File;

/**
 * Defines an artifacts version
 */
public interface ArtifactVersion {
    /**
     *
     * @return the artifacts group id
     */
    String getGroupId();

    /**
     *
     * @return the artifacts version
     */
    String getArtifactId();

    /**
     *
     * @return reference to the artifact group
     */
    Group getGroup();

    /**
     *
     * @return reference to the artifact
     */
    Artifact getArtifact();

    /**
     *
     * @return the artifacts version
     */
    String getVersion();

    /**
     *
     * @return artifact version path with form /groupId/artifactId/version/fileName
     */
    String getPath();

    /**
     *
     * @return optional pdf file, null if none was found
     */
    File getPdfFile();

    /**
     * Check release version pattern "[1-9]{1}[0-9]*.[0-9]*.[0-9]*"
     *
     * @return true if release version, false otherwise
     */
    boolean isRelease();
}
